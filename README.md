# weeks

This is a collection of projects I will be making every week for every year until I decide I am good enough at programming.

Here are the languages I will write these programs in:

- C/C++
- Python
- Rust
- PHP(aka goodlang aka garbage) w/ JS
- Lua w/ Luarocks
- Shell script

Project structure

|--------|------------------|
|what.txt|Explains project  |
|src     |Source code       |
|Makefile|Builds the project|
|--------|------------------|
